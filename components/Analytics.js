import moment from "moment";

import {
  Input,
  Typography,
  Select,
  Image,
  Tabs,
  DatePicker,
  Tag,
  Divider,
  Statistic,
  Button,
} from "antd";

import { useState } from "react";

import { useQuery } from "urql";

const { Option } = Select;
const { RangePicker } = DatePicker;

export default function AnalyticsComponent() {
  // States
  const [range, setRange] = useState({
    start: null,
    end: null,
  });
  const [customer, setCustomer] = useState(null);
  const [product, setProduct] = useState(null);

  // Requests
  const GET_ORDERS = `
        query GET_ORDERS{
          getOrders{
            id       
            status
            deliver
            disbursed
            packed 
            payment{
              ref
              timestamp
              amount
            }
            customer{
              id
              displayName
              phoneNumber
              location
            }
            products{
              id
              meta{
                id
                image
                name
                price
              }
              quantity
            } 
            disburseDate
            createdAt
            updatedAt
          }
        }
      `;

  const [{ data: oData, fetching: oFetching, error: oError }, reexeccuteQuery] =
    useQuery({
      query: GET_ORDERS,
    });

  // Functions
  const getProductCount = () => {
    let count = 0;
    let products = [];

    function onlyUnique(value, index, self) {
      return self.indexOf(value) === index;
    }

    oData?.getOrders
      .filter((order) => {
        if (order.disburseDate && !range.start && !range.end) {
          return order.disburseDate;
        }
        if (order.disburseDate && range.start && range.end) {
          return (
            order.disburseDate &&
            parseInt(order.disburseDate) > range.start &&
            parseInt(order.disburseDate) < range.end
          );
        }
      })
      .forEach((order) => {
        order?.products.forEach((product) => {
          count = count + product?.quantity;
          products.push(product?.meta?.name);
        });
      });

    return { count, products: products.filter(onlyUnique) };
  };

  const getSalesEarnings = () => {
    let sales = 0;
    oData?.getOrders
      .filter((order) => {
        if (order.disburseDate && !range.start && !range.end) {
          return order.disburseDate;
        }
        if (order.disburseDate && range.start && range.end) {
          return (
            order.disburseDate &&
            parseInt(order.disburseDate) > range.start &&
            parseInt(order.disburseDate) < range.end
          );
        }
      })
      .forEach((order) => {
        order?.products.forEach((product) => {
          if (!product?.meta?.price) {
            return;
          }
          sales = sales + product?.quantity * product?.meta?.price;
        });
      });
    return sales;
  };

  const getCustomerReach = () => {
    let customers = [];

    function onlyUnique(value, index, self) {
      return self.indexOf(value) === index;
    }

    oData?.getOrders
      .filter((order) => {
        if (order.disburseDate && !range.start && !range.end) {
          return order.disburseDate;
        }
        if (order.disburseDate && range.start && range.end) {
          return (
            order.disburseDate &&
            parseInt(order.disburseDate) > range.start &&
            parseInt(order.disburseDate) < range.end
          );
        }
      })
      .forEach((order, i) => {
        customers.push(order.customer.displayName);
      });

    return customers.filter(onlyUnique);
  };

  let stats = {
    earnings: getSalesEarnings(),
    products: getProductCount().count,
    orders: oData?.getOrders.filter((order) => {
      if (order.disburseDate && !range.start && !range.end) {
        return order.disburseDate;
      }
      if (order.disburseDate && range.start && range.end) {
        return (
          order.disburseDate &&
          parseInt(order.disburseDate) > range.start &&
          parseInt(order.disburseDate) < range.end
        );
      }
    }).length,
    customers: getCustomerReach().length,
  };

  const getSales = () => {
    let sales = [];

    oData?.getOrders
      .filter((order) => {
        if (order.disburseDate && !range.start && !range.end) {
          return order.disburseDate;
        }
        if (order.disburseDate && range.start && range.end) {
          return (
            order.disburseDate &&
            parseInt(order.disburseDate) > range.start &&
            parseInt(order.disburseDate) < range.end
          );
        }
      })
      .forEach((order) => {
        order.products.forEach((product) => {
          let sale = {
            name: product?.meta?.name,
            quantity: product.quantity,
            customer: order.customer.displayName,
            phoneNumber: order.customer.phoneNumber,
            location: order.customer.location,
            ref: order.payment.ref,
            amount: order.payment.amount,
            ordered: order.createdAt,
            disbursed: order.disburseDate,
            image: product?.meta?.image,
          };

          sales.push(sale);
        });
      });

    return sales;
  };

  return (
    <div>
      <div>
        <RangePicker
          showTime
          className="w-full"
          use12Hours
          onChange={(a, b) => {
            if (!a) {
              setRange({
                start: null,
                end: null,
              });
              return;
            }

            setRange({
              start: new Date(a[0]).getTime(),
              end: new Date(a[1]).getTime(),
            });
          }}
        />
      </div>
      <Divider orientation="right">Stats</Divider>
      <div className="grid gap-4 grid-cols-2 sm:grid-cols-4 ">
        <Statistic
          title="Sales earnings"
          value={`Ksh ${stats.earnings}`}
          className="col-span-1"
        />
        <Statistic
          title="Products sold"
          value={`${stats.products}`}
          className="col-span-1"
        />
        <Statistic
          title="Orders made"
          value={stats.orders}
          className="col-span-1"
        />
        <Statistic
          title="Customer reach"
          value={stats.customers}
          className="col-span-1"
        />
      </div>

      <Divider orientation="right">Sales</Divider>
      <div className="flex gap-2 my-2">
        <Select
          allowClear
          showSearch
          className="w-[100%]"
          placeholder="Product"
          optionFilterProp="children"
          onChange={(val) => setProduct(val)}
          filterOption={(input, option) => option.children.includes(input)}
          filterSort={(optionA, optionB) =>
            optionA.children
              ?.toLowerCase()
              ?.localeCompare(optionB.children?.toLowerCase())
          }
        >
          {getProductCount().products.map((product, i) => (
            <Option key={i} value={product}>
              {product}
            </Option>
          ))}
        </Select>
      </div>
      <div className="w-full flex justify-end">
        <Button type="link">
          {
            getSales().filter((sale) => {
              if (customer) {
                return sale.customer == customer;
              }
              if (product) {
                return sale.name == product;
              }
              return sale;
            }).length
          }{" "}
          sale(s)
        </Button>
      </div>
      {getSales()
        .filter((sale) => {
          if (customer) {
            return sale.customer == customer;
          }
          if (product) {
            return sale.name == product;
          }
          return sale;
        })
        .map((sale, i) => {
          return (
            <div
              key={i}
              className="p-2 border-solid border-2 border-indigo my-1 flex"
            >
              <Image
                height={56}
                width={56}
                className="object-contain"
                src={sale.image}
              />
              <div className="mx-4">
                <p className="p-0 m-0">
                  <strong>{sale?.name}</strong> x{" "}
                  <Tag color={"green"}>{sale?.quantity}</Tag>
                </p>
                <div className="flex justify-between w-full space-x-6">
                  <p className="text-green">{sale?.customer}</p>
                  <p>{sale?.phoneNumber}</p>
                  <p className="text-green">{sale?.location}</p>
                </div>
                <p>
                  <span>
                    <Tag color="green">{sale?.ref}</Tag> |{" "}
                    <code>Ksh. {sale?.amount}</code>
                  </span>
                </p>
                <p className="p-0 m-0">
                  Ordered :{" "}
                  <span>
                    {moment(new Date(parseInt(sale?.ordered))).format(
                      "Do MM/YY  @ h:mm a"
                    )}
                  </span>
                </p>
                <p className="p-0 m-0">
                  Disbursed :{" "}
                  <span>
                    {moment(new Date(parseInt(sale?.disbursed))).format(
                      "Do MM/YY  @ h:mm a"
                    )}
                  </span>
                </p>
              </div>
            </div>
          );
        })}
    </div>
  );
}
