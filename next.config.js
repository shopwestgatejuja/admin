const withAntdLess = require("next-plugin-antd-less");
const withPlugins = require("next-compose-plugins");

// optional next.js configuration
const nextConfig = {
  env: {
    CLOUD_NAME: "westgatemart",
    API_KEY: "338747667265387",
    API_SECRET: "rRcXR8-OwZel3xmDCV6o3lwzt4M",
  },
  reactStrictMode: true,
  images: {
    domains: ["firebasestorage.googleapis.com"],
  },
  webpack5: true,
  webpack: (config) => {
    config.resolve.fallback = { fs: false };
    return config;
  },
  module: {
    loaders: [
      {
        test: /\.css$/,
        loaders: ['style?insertAt=top', 'css'],
      },
    ],
  },
};

module.exports = withPlugins(
  [
    // another plugin with a configuration
    [
      withAntdLess,
      {
        // optional: you can modify antd less variables directly here
        modifyVars: { "@primary-color": "#3F9B42" },
        // Or better still you can specify a path to a file
        // lessVarsFilePath: "./styles/variables.less",
        // optional
        lessVarsFilePathAppendToEndOfContent: false,
        // optional https://github.com/webpack-contrib/css-loader#object
        cssLoaderOptions: {},
      },
    ],
  ],
  nextConfig
);
