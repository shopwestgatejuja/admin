import moment from "moment";
import { FaPlus } from "react-icons/fa";
import { client } from "../helpers/algolia-search.js";
import {
  InstantSearch,
  Hits,
  RefinementList,
  ToggleRefinement,
  Pagination,
  Configure,
  connectHits,
  SearchBox,
  connectSearchBox,
} from "react-instantsearch-dom";

import {
  Input,
  Form,
  Typography,
  InputNumber,
  Select,
  Empty,
  message,
  Row,
  Image,
  Col,
  Tabs,
  Table,
  Avatar,
  Popconfirm,
  DatePicker,
  Tag,
  Divider,
  Menu,
  Statistic,
  Dropdown,
  Modal,
  Button,
  Card,
  Spin,
  Popover,
  Tooltip,
  Badge,
} from "antd";

import { useState, useEffect, useRef } from "react";

import { useQuery, useMutation, useClient } from "urql";

import {
  FilterOutlined,
  PlusOutlined,
  DownOutlined,
  InfoCircleOutlined,
} from "@ant-design/icons";
import Compressor from "compressorjs";

import { useRouter } from "next/router";

import AdminComponent from "../components/Admins.js";
import ProductsComponent from "../components/Products.js";
import OrdersComponent from "../components/Orders.js";
import AnalyticsComponent from "../components/Analytics.js";

const { Option } = Select;
const { RangePicker } = DatePicker;
const { TabPane } = Tabs;
const { Title, Text, Paragraph } = Typography;
const { Search } = Input;

const Login = () => {
  // States & refs
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);
  const router = useRouter();

  // Requests
  const GET_ADMINS = `
   query GET_ADMINS{
     getAdmins{
       id
       username
       password
       telephone
       removed
     }
   }
 `;

  const [{ data, fetching, error }, reexeccuteQuery] = useQuery({
    query: GET_ADMINS,
  });

  // Functions

  const handleSubmit = (e) => {
    setLoading(true);
    e.preventDefault();

    const payload = {
      username,
      password,
    };

    const adminExists = (payload) => {
      message.success(`Welcome back , ${payload?.username} `);
      setLoading(false);
      localStorage.setItem("admin", JSON.stringify(payload));
      router.reload();
    };

    if (data?.getAdmins) {
      data?.getAdmins.filter(
        (admin) =>
          admin.username == payload.username &&
          admin.password == payload.password &&
          !admin.removed
      ).length > 0
        ? adminExists(payload)
        : message.info(`No such admin exists`);

      setLoading(false);
    } else {
      message.error(`Something went wrong`);
    }
  };

  return (
    <div className="bg-[#3F9B42] h-screen w-screen relative">
      <div className="bg-white p-4 h-auto absolute top-[30%] left-[50%] translate-x-[-50%] w-[300px] items-center">
        <Image
          src="/logo.jpeg"
          width={53.1}
          height={38.25}
          className="absolute left-[150px] translate-x-[-50%] "
          style={{ margin: "0 auto", borderRadius: "50%" }}
        />
        <p className="w-full text-center uppercase text-[#3F9B42] text-lg tracking-tighter">
          Westgate shop portal
        </p>

        <Input
          type="text"
          style={{ margin: "12px 0px" }}
          required
          value={username}
          placeholder="Username"
          onChange={(e) => setUsername(e.target.value)}
        />
        <Input.Password
          required
          value={password}
          placeholder="Password"
          onChange={(e) => setPassword(e.target.value)}
        />
        <Button
          loading={loading}
          type="primary"
          className="bg-[#3F9B42] p-2 outline-none rounded-md w-full uppercase text-gray-50 my-4"
          onClick={handleSubmit}
        >
          login
        </Button>
      </div>
    </div>
  );
};

const Console = () => {
  const [admin, setAdmin] = useState(null);
  const router = useRouter();

  useEffect(() => {
    if (localStorage.getItem("admin") && !admin) {
      let _admin = localStorage.getItem("admin");
      let _admin_ = JSON.parse(_admin);
      setAdmin(_admin_);
      return;
    }
  }, []);

  if (!admin) {
    return <Login />;
  }

  const handleLogOut = () => {
    localStorage.clear();
    setAdmin(null);
    router.reload();
  };

  return (
    <div>
      <Header logged={admin} logOut={handleLogOut} />
      <Dashboard logged={admin} />
    </div>
  );
};

const Header = ({ logged, logOut }) => {
  // States
  const [cpModal, setCPModal] = useState(false);
  const graphqlClient = useClient();
  const router = useRouter();

  const [cPassword, setCPassword] = useState("");
  const [nPassword, setNPassword] = useState("");
  const [rPassword, setRPassword] = useState("");

  // Requests
  const CHANGE_PASSWORD = `
    mutation CHANGE_PASSWORD(
      $id: ID!
        $password:String!
    ){
      changeAdminPassword(id: $id 
          password: $password
        ){
          id
        }
    }
  `;

  const GET_ADMINS = `
  query GET_ADMINS{
    getAdmins{
      id
      username
      password
      telephone
      removed
    }
  }
`;

  const [mResults, _changePassword] = useMutation(CHANGE_PASSWORD);

  // Functions
  const handleChangePassword = () => {
    if (rPassword !== nPassword) {
      message.warn(`Passwords do not match`);
      return;
    }

    graphqlClient
      .query(GET_ADMINS)
      .toPromise()
      .then(({ data }) => {
        if (data?.getAdmins) {
          data.getAdmins.filter(
            (admin) =>
              admin.password == cPassword && admin.username == logged?.username
          ).length == 0
            ? message.info(`Wrong current password entered`)
            : _changePassword({
                id: data.getAdmins.filter(
                  (admin) =>
                    admin.password == cPassword &&
                    admin.username == logged?.username
                )[0].id,
                password: nPassword,
              }).then(({ data }) => {
                if (data?.changeAdminPassword) {
                  message.success("Password successfully changed");
                  localStorage.setItem(
                    "admin",
                    JSON.stringify({
                      username: logged?.username,
                      password: nPassword,
                    })
                  );
                  router.reload();
                }
              });
        }
      });
  };

  const content = (
    <div>
      <Button
        onClick={() => setCPModal(true)}
        type="link"
        className="w-full text-center my-2"
      >
        Change password
      </Button>
      <Button onClick={logOut} type="primary" className="w-full">
        Log out
      </Button>
    </div>
  );

  return (
    <div className="flex items-center justify-between p-4">
      <div>
        <h1 className="text-xl text-[#3F9B42]">Dashboard</h1>
      </div>
      <div>
        <Popover
          placement="bottom"
          title={null}
          content={content}
          trigger="click"
        >
          <Avatar>{logged?.username.toUpperCase().slice(0, 2)}</Avatar>
        </Popover>
      </div>

      <Modal
        visible={cpModal}
        onOk={handleChangePassword}
        onCancel={() => {
          setCPModal(false);
        }}
        title="Change password"
      >
        <div className="space-y-3">
          <Input.Password
            value={cPassword}
            placeholder="Current password"
            onChange={(e) => setCPassword(e.target.value)}
          />

          <Input.Password
            value={nPassword}
            placeholder="New password"
            onChange={(e) => setNPassword(e.target.value)}
          />

          <Input.Password
            value={rPassword}
            placeholder="Repeat password"
            onChange={(e) => setRPassword(e.target.value)}
          />

          {nPassword && rPassword && nPassword !== rPassword && (
            <p className="text-red-700">
              <InfoCircleOutlined /> Repeated password should match new password
            </p>
          )}
        </div>
      </Modal>
    </div>
  );
};

const Dashboard = () => {
  return (
    <div className="px-4">
      <Tabs defaultActiveKey="1">
        {/* <TabPane tab="Dashboard" key="1"></TabPane> */}
        <TabPane tab="Products" key="2">
          <ProductsComponent />
        </TabPane>
        <TabPane tab="Orders" key="3">
          <OrdersComponent />
        </TabPane>
        <TabPane tab="Analytics" key="4">
          <AnalyticsComponent />
        </TabPane>
        <TabPane tab="Admin" key="5">
          <AdminComponent />
        </TabPane>
      </Tabs>
    </div>
  );
};

export default Console;
