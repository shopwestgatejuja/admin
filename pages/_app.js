import "../styles/globals.css";
import "antd/dist/antd.css";
import { createClient, Provider } from "urql";
import { createContext, useState } from "react";

export const AuthContext = createContext();

export const client = createClient({
  url: "https://westgate-shop.herokuapp.com/graphql",
});

function MyApp({ Component, pageProps }) {
  const [admin, setAdmin] = useState(null);

  return (
    <Provider value={client}>
      <AuthContext.Provider value={[admin, setAdmin]}>
        <Component {...pageProps} />
      </AuthContext.Provider>
    </Provider>
  );
}

export default MyApp;
